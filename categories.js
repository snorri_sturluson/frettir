var categories = [
    {
        name: "verkfall",
        title: "Verkföll og kjaramál",
        keywords: ["verkfall", "verkföll", "kjaradeil", "kjararáð", "launadeil", "kjarasamning"]
    },
    {
        name: "flottamenn",
        title: "Flóttamenn",
        keywords: ["flóttamenn", "flóttamann", "flóttamaður"]
    },
    {
        name: "virkjanir",
        title: "Virkjanir",
        keywords: ["virkjun", "virkjanir", "virkjana", "tilraunahol"]
    },
    {
        name: "forsetakosningar",
        title: "Forsetakosningar",
        keywords: ["forsetakosning", "forsetaframboð"]
    }
];

require("fs").writeFileSync("public/categories.json", JSON.stringify(categories, null, 4));

function getCombined() {
    var combined = new Set();
    for(var each of categories) {
        for(var kw of each.keywords) {
            combined.add(kw);
        }
    }

    return Array.from(combined.values());
}

module.exports = {
    categories: categories,
    getCombined: getCombined
};
