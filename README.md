# Fréttir #

This project gathers news from Icelandic websites that provide an RSS feed and categorizes the news items. The main purpose of this project is for the author to get some experience running a web site on AWS, and implementing a website with React.

The website itself is served from a static Amazon S3 bucket - there is no web server per se. The website is a single page app implemented using react-bootstrap and requests data in json format. There is no server to respond to those requests - they are simply json files that are populated with periodic runs of a scanner. The scanner writes data into a DynamoDB database, then does a query that writes results into the json files used by the website.
