function get(url, done) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function () {
        if (this.readyState == 1) {
            httpRequest.setRequestHeader("Accept", "application/json")
            httpRequest.setRequestHeader("Content-Type", "application/json");
        }
    };
    httpRequest.onload = function () {
        if (httpRequest.status >= 200 && httpRequest.status < 300) {
            var obj = null;
            try {
                obj = JSON.parse(httpRequest.responseText);
            }
            catch(err) {
            }
            done(null, obj);
        } else {
            done(httpRequest.status, null);
        }
    };

    httpRequest.open("GET", url, true);
    httpRequest.send(null);
};

function post(url, data, done) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function () {
        if (this.readyState == 1) {
            httpRequest.setRequestHeader("Content-Type", "application/json");
            httpRequest.send(JSON.stringify(data));
        }
    };
    httpRequest.onload = function () {
        if (httpRequest.status == 200) {
            var obj = null;
            try {
                obj = JSON.parse(httpRequest.responseText);
            }
            catch(err) {
            }
            done(null, obj);
        } else {
            done(httpRequest.status, null);
        }
    };
    httpRequest.open("POST", url, true);
};

module.exports.get = get;
module.exports.post = post;