"use strict";
var request = require("../json-request");
var React = require("react");
var Link = require("react-router").Link;
var Well = require("react-bootstrap/lib/Well");

var CategoryInfo = React.createClass({
    getLink: function() {
        return this.props.category.name + ".json";
    },

    render: function() {
        return (
            <div className="levelInfo">
                <h2><Link to={"news/" + this.props.category.name}>{this.props.category.title}</Link></h2>
            </div>
        )
    }
});

var CategoryList = React.createClass({
    getInitialState: function() {
        return {
            data: []
        }
    },

    componentDidMount: function() {
        request.get("categories.json", function (err, res) {
            if(err) {
                console.log(err);
                return;
            }
            this.setState({data: res});
        }.bind(this));
    },

    render: function() {
        var categoryInfoNodes = this.state.data.map(function(category) {
            return (
                <CategoryInfo key={category.name} category={category}/>
            );
        }.bind(this));

        return (
            <Well>
                {categoryInfoNodes}
            </Well>
        );
    }
});

module.exports = CategoryList;