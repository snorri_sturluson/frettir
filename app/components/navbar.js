"use strict";
var React = require("react");
var PropTypes = React.PropTypes;

var Link = require("react-router").Link;

var Navbar = require("react-bootstrap/lib/Navbar");
var Nav = require("react-bootstrap/lib/Nav");
var Glyphicon = require("react-bootstrap/lib/Glyphicon");

var ReactRouterBootstrap = require('react-router-bootstrap');
var NavItemLink = ReactRouterBootstrap.NavItemLink;


var AppNavbar = React.createClass({
  displayName: "AppNavbar",
  propTypes: {
    brand: PropTypes.string,
  },
  renderBrand: function () {
    return (<Link to="index">{this.props.brand}</Link>);
  },
  renderNavLinks: function () {
    return (
      <Nav right eventKey={0}>
        <NavItemLink eventKey={1} to="profile">
          <Glyphicon glyph="user" /> {this.state.user.username}
        </NavItemLink>
        <NavItemLink to="sign-out">
          <Glyphicon glyph="off" /> Sign out
        </NavItemLink>
      </Nav>
    );
  },
  render: function () {
    return (
      <Navbar brand={this.renderBrand()} inverse fixedTop>
        {this.renderNavLinks()}
      </Navbar>
    );
  }
});

module.exports = AppNavbar;
