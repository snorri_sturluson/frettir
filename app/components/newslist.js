"use strict";
var request = require("../json-request");
var urls = require("../urls");
var React = require("react");
var Button = require("react-bootstrap/lib/Button");
var Well = require("react-bootstrap/lib/Well");
var Grid = require("react-bootstrap/lib/Grid");
var Row = require("react-bootstrap/lib/Row");
var Col = require("react-bootstrap/lib/Col");
var Badge = require("react-bootstrap/lib/Badge");
var Image = require("react-bootstrap/lib/Image");
var Panel = require("react-bootstrap/lib/Panel");
var Glyphicon = require("react-bootstrap/lib/Glyphicon");
var timeago = require("../../timeago.js");
var LinkContainer = require("react-router-bootstrap").LinkContainer;
var Link = require("react-router").Link;

var MAX_DESCRIPTION_LENGTH = 600;

function getLocation(href) {
    var match = href.match(/^(https?\:)\/\/(([^:\/?#]*)(?:\:([0-9]+))?)(\/[^?#]*)(\?[^#]*|)(#.*|)$/);
    return match && {
            protocol: match[1],
            host: match[2],
            hostname: match[3],
            port: match[4],
            pathname: match[5],
            search: match[6],
            hash: match[7]
        }
}

var icons = {
    "dv.is": "http://www.dv.is/favicon.ico",
    "ruv.is": "http://www.ruv.is/sites/all/themes/at_ruv/favicon.ico",
    "vb.is": "http://www.vb.is/static/img/vb.ico",
    "pressan.is": "http://www.pressan.is/favicon.ico",
    "eyjan.pressan.is": "http://www.pressan.is/favicon.ico",
    "mbl.is": "http://www.mbl.is/favicon.ico"
};

var NewsItem = React.createClass({
    handleSelect: function() {
        this.props.onSelect(this.props.data);
    },

    getDescription: function() {
        var description = this.props.data.description.trim();
        if(description.length > MAX_DESCRIPTION_LENGTH) {
            description = description.substr(0, MAX_DESCRIPTION_LENGTH) + "..."
        }
        return description;
    },

    getNavigateToLink: function() {
        window.location.href=this.props.data.link;
    },

    getIcon: function() {
        var location = getLocation(this.props.data.link);
        var host = location.host;
        if(host.startsWith("www.")) {
            host = host.substr(4);
        }
        return icons[host];
    },

    render: function() {
        return (
            <Col sm={6} min-height="150px">
                <Panel onClick={this.getNavigateToLink}>
                    <Image src={this.getIcon()} height="16" width="16"/>{' '}
                    <b><a href={this.props.data.link}>{this.props.data.title}</a></b>
                    <Badge pullRight={true}>{timeago(this.props.data.pubDate)}</Badge>
                    <br/>
                    {this.getDescription()}
                </Panel>
            </Col>
        );
    }
});

var NewsList = React.createClass({
    displayName: "NewsList",

    getInitialState: function() {
        return {
            data: [],
            category: {},
            prev: null,
            next: null
        }
    },

    componentDidMount: function() {
        request.get("categories.json", function(err, res) {
            if(err) {
                console.log(err);
                return;
            }
            var n = res.length;
            for(var i = 0; i < n; i++) {
                var each = res[i];
                var prev = null;
                if( i > 0) {
                    prev = res[i-1].name;
                }
                var next = null;
                if(i < n-1) {
                    next = res[i+1].name;
                }
                if(each.name == this.props.params.category) {
                    this.setState({
                        category: each,
                        prev: prev,
                        next: next
                    });
                    break;
                }
            }
        }.bind(this));
        request.get(this.props.params.category + ".json", function (err, res) {
            if(err) {
                console.log(err);
                return;
            }
            this.setState({data: res});
        }.bind(this));
    },

    getPrevLink: function() {
        if(this.state.prev) {
            return(
                <div>
                    <Link to={"news/" + this.state.prev}>Fyrri</Link>
                </div>
            )
        }
        else
        {
            return(
                <Button disabled><Glyphicon glyph="chevron-left"/></Button>
            )
        }
    },

    getNextLink: function() {
        return(
            <Button><Glyphicon glyph="chevron-right"/></Button>
        )
    },

    render: function() {
        var rows = [];
        var count = this.state.data.length;
        for(var i = 0; i < count; i += 2) {
            var newsItemNodes = [];
            var curData = this.state.data[i];
            newsItemNodes.push(<NewsItem key={curData.link} data={curData}/>);
            if(i + 1 < count ) {
                curData = this.state.data[i+1];
                newsItemNodes.push(<NewsItem key={curData.link} data={curData}/>);
            }
            rows.push(
                <Row key={i} className="show-grid">
                    {newsItemNodes}
                </Row>
            );
        }

        return (
            <div>
                <h2>{this.state.category.title}</h2>
                <Grid>
                    {rows}
                </Grid>
            </div>
        );
    }
});

module.exports = NewsList;
