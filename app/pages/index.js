"use strict";
var React = require("react");
var Nav = require("react-bootstrap/lib/Nav");
var ReactRouterBootstrap = require('react-router-bootstrap');
var NavItemLink = ReactRouterBootstrap.NavItemLink;

module.exports = React.createClass({
    displayName: "IndexPage",
    render: function () {
        return (
            <div>
                <h2>
                    <Nav bsStyle="pills" bsSize="large" stacked>
                    </Nav>
                </h2>
            </div>
        );
    }
});
