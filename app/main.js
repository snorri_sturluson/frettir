var React = require("react");
var ReactRouter = require("react-router");
var ReactDom = require("react-dom");

var NewsList = require("./components/newslist.js");
var CategoryList = require("./components/categorylist.js");
var PageHeader = require("react-bootstrap/lib/PageHeader");
var Glyphicon = require("react-bootstrap/lib/Glyphicon");
var Button = require("react-bootstrap/lib/Button");

var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
var IndexRoute = ReactRouter.IndexRoute;
var IndexLink = ReactRouter.IndexLink;
var Link = ReactRouter.Link;
var LinkContainer = require("react-router-bootstrap").LinkContainer;
var IndexLinkContainer = require("react-router-bootstrap").IndexLinkContainer;

var App = React.createClass({
    displayName: "App",

    render: function () {
        return (
            <div className="container">
                <PageHeader>
                    <IndexLinkContainer to="/">
                        <Button><Glyphicon glyph="home"/></Button>
                    </IndexLinkContainer>
                    {' '}
                    Fréttayfirlit
                    <span className="pull-right">
                        <LinkContainer to="info">
                            <Button><Glyphicon glyph="info-sign"/></Button>
                        </LinkContainer>
                    </span>
                </PageHeader>
                {this.props.children}
            </div>
        );
    }
});

var Info = React.createClass({
    render: function() {
        return (
            <div>
                <p>Þessi vefsíða birtir flokkaðar fréttir úr íslenskum fjölmiðlum sem bjóða
                upp á fréttaveitu á RSS sniði.</p>
                <p>Tilgangur síðunnar er fyrst og fremst til að höfundur öðlist reynslu
                í að keyra vefþjónustu með tækni frá Amazon.</p>
                <p>Forritstextann má nálgast á <a href="https://bitbucket.org/snorri_sturluson/frettir">Bitbucket</a></p>
                <p>Athugasemdir eða spurningar má senda í <a href="mailto:snorri.sturluson+frettayfirlit@gmail.com">
                    tölvupósti</a>.</p>
            </div>
        )
    }
});

ReactDom.render((
    <Router>
        <Route path="/" component={App}>
            <IndexRoute component={CategoryList}/>
            <Route path="news/:category" component={NewsList}/>
            <Route path="info" component={Info}/>
        </Route>
    </Router>
), document.getElementById("content"))

