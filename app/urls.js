var SERVER = "https://s3-eu-west-1.amazonaws.com/verkfall/";
function get(name) {
    return SERVER + name + ".json";
}

module.exports = get;