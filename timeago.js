module.exports = function(then) {
    var now = Date.now();
    var sec = Math.floor((now - then) / 1000);

    if(sec < 0) {
        return "framtíð";
    }
    if(sec < 2) {
        return "núna";
    }
    if(sec < 60) {
        return sec.toString() + "s";
    }
    var min = Math.floor(sec / 60);
    if(min < 60) {
        return min.toString() + "m";
    }

    var hr = Math.floor(min / 60);
    if(hr < 25) {
        return hr.toString() + "klst";
    }
    var date = new Date(then);
    date.setHours(12, 0, 0, 0);
    var today = new Date(now);
    today.setHours(12, 0, 0, 0);

    var days = Math.floor((today - date) / (24*60*60*1000));
    if(days < 2) {
        return "í gær";
    }
    if(days < 3) {
        return "í fyrradag";
    }
    if(days < 14) {
        return "fyrir " + days.toString() + " dögum";
    }

    return "fyrir löngu";
};