var AWS = require('aws-sdk');
try {
    AWS.config.loadFromPath('./config.json');
}
catch(e) {
    AWS.config.region = "eu-west-1";
}
var fs = require("fs");
var dd = new AWS.DynamoDB();
var docClient = new AWS.DynamoDB.DocumentClient();

var desc = JSON.parse(fs.readFileSync("table.json"));
console.log(desc);

dd.deleteTable({TableName:'links-visited'}, function(err, result) {
    dd.createTable(desc, function(err, result) {
        if (err) console.log(err, err.stack);
        else console.log(JSON.stringify(result, null, 4));
    });
});
