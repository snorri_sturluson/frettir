var AWS = require('aws-sdk');
try {
    AWS.config.loadFromPath('./config.json');
}
catch(e) {
    AWS.config.region = "eu-west-1";
}
var docClient = new AWS.DynamoDB.DocumentClient();

var fs = require("fs");
var Promise = require("bluebird");
var categories = require("./categories.js");
var description = require("./description.js");

Promise.promisifyAll(docClient);

function saveCategory(name, items)
{
    for(var each of items) {
        each.image = description.extractImage(each.description);
        each.description = description.extractText(each.description);
    }
    fs.writeFileSync("public/" + name + ".json", JSON.stringify(items, null, 4));
}

function splitResults(results) {
    var split = {};
    for(var each of categories.categories) {
        split[each.name] = {
            items: [],
            keywords: new Set(each.keywords)
        };
    }
    for(var each of results) {
        var keywords = each.keywords.split(",");
        for(var name in split) {
            var cat = split[name];
            for(var kw of keywords) {
                if(cat.keywords.has(kw)) {
                    cat.items.push(each);
                    break;
                }
            }
        }
    }
    for(var name in split) {
        saveCategory(name, split[name].items);
    }
}

var params = {
    TableName: "links-visited",
    IndexName: "isMatch-pubDate-index",
    KeyConditionExpression: "isMatch = :isMatch",
    ExpressionAttributeValues: {
        ":isMatch": "true",
    },
    ScanIndexForward: false
};
docClient.queryAsync(params)
    .then(function(result) {
        splitResults(result.Items);
    });
