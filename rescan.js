//add timestamps in front of log messages
require('console-stamp')(console, '[dd/mm/yyyy HH:MM:ss.l]');

var AWS = require('aws-sdk');
try {
    AWS.config.loadFromPath('./config.json');
}
catch(e) {
    AWS.config.region = "eu-west-1";
}

var processor = require("./processor.js")
var categories = require("./categories.js");

var processor = new processor.Processor();
processor.keywords = categories.getCombined();
processor.rescanLinks();