var xmldoc = require("xmldoc");
var striptags = require("striptags");

function extractImage(desc) {
    desc = desc.trim();
    if(desc.startsWith("<img")) {
        var img = desc.split(">")[0] + ">";
        var imgdoc = new xmldoc.XmlDocument(img);
        return imgdoc.attr.src;
    }
    return null;
}

function extractText(desc) {
    return striptags(desc);
}

module.exports = {
    extractImage: extractImage,
    extractText: extractText
};