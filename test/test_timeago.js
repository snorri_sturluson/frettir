var test = require("unit.js");
var timeago = require("../timeago.js");

describe("timeago", function() {
    it("Núna", function() {
        var actual = timeago(Date.now());
        var expected = "núna";
        test.assert.equal(actual, expected);
    });

    it("5s", function() {
        var actual = timeago(Date.now() - 5000);
        var expected = "5s";
        test.assert.equal(actual, expected);
    });

    it("59s", function() {
        var actual = timeago(Date.now() - 59900);
        var expected = "59s";
        test.assert.equal(actual, expected);
    });

    it("2m", function() {
        var actual = timeago(Date.now() - 2*60*1000);
        var expected = "2m";
        test.assert.equal(actual, expected);
    });

    it("59m", function() {
        var actual = timeago(Date.now() - (59*60*1000 + 900));
        var expected = "59m";
        test.assert.equal(actual, expected);
    });

    it("59m", function() {
        var actual = timeago(Date.now() - (59*60*1000 + 900));
        var expected = "59m";
        test.assert.equal(actual, expected);
    });

    it("1klst", function() {
        var actual = timeago(Date.now() - (60*60*1000));
        var expected = "1klst";
        test.assert.equal(actual, expected);
    });

    it("24klst", function() {
        var actual = timeago(Date.now() - (24*60*60*1000));
        var expected = "24klst";
        test.assert.equal(actual, expected);
    });

    it("í gær", function() {
        var actual = timeago(Date.now() - (28*60*60*1000));
        var expected = "í gær";
        test.assert.equal(actual, expected);
    });

    it("í fyrradag", function() {
        var actual = timeago(Date.now() - (50*60*60*1000));
        var expected = "í fyrradag";
        test.assert.equal(actual, expected);
    });

    it("fyrir 4 dögum", function() {
        var actual = timeago(Date.now() - (4*24*60*60*1000));
        var expected = "fyrir 4 dögum";
        test.assert.equal(actual, expected);
    });
});