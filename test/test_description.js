var test = require("unit.js");
var description = require("../description.js");

describe("description", function() {
    it("extractImage returns null on empty string", function() {
        var actual = description.extractImage("");
        test.assert.equal(actual, null);
    });

    it("extractImage returns image on valid string with only image tag", function() {
        var actual = description.extractImage("<img src=\"http://www.mbl.is/frimg/8/60/860372A.jpg\"/>");
        test.assert.equal(actual, "http://www.mbl.is/frimg/8/60/860372A.jpg");
    });

    it("extractImage returns image on valid string with only image tag with other attributes", function() {
        var actual = description.extractImage("<img style=\"margin:0 1em 1em 0;\" align=\"left\" alt=\"Frá Stavangri.\"     src=\"http://www.mbl.is/frimg/8/60/860372A.jpg\"/>");
        test.assert.equal(actual, "http://www.mbl.is/frimg/8/60/860372A.jpg");
    });

    it("extractImage returns image on valid string with image tag and other text", function() {
        var desc = "  <img style=\"margin:0 1em 1em 0;\" align=\"left\" alt=\"Frá Stavangri.\"     src=\"http://www.mbl.is/frimg/8/60/860372A.jpg\"/>\nLögreglan í Stafangri rannsakar nú hvort þriggja ára gömlum dreng hafi verið nauðgað í flóttamannamiðstöð í borginni.\n";
        var actual = description.extractImage(desc);
        test.assert.equal(actual, "http://www.mbl.is/frimg/8/60/860372A.jpg");
    });

    it("extractImage returns null on string with broken image tag and other text", function() {
        var desc = "  <mg style=\"margin:0 1em 1em 0;\" align=\"left\" alt=\"Frá Stavangri.\"     src=\"http://www.mbl.is/frimg/8/60/860372A.jpg\"/>\nLögreglan í Stafangri rannsakar nú hvort þriggja ára gömlum dreng hafi verið nauðgað í flóttamannamiðstöð í borginni.\n";
        var actual = description.extractImage(desc);
        test.assert.equal(actual, null);
    });

    it("extractText returns empty string on empty string", function() {
        var actual = description.extractText("");
        test.assert.equal(actual, "");
    });

    it("extractText returns string on plain string", function() {
        var img = "<img src=\"http://www.mbl.is/frimg/8/60/860372A.jpg\"/>";
        var desc = "This is a simple test";
        var actual = description.extractText(img + desc);
        test.assert.equal(actual, desc);
    });


});