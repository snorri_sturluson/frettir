var AWS = require('aws-sdk');
var dd = new AWS.DynamoDB();
var docClient = new AWS.DynamoDB.DocumentClient();

var Promise = require("bluebird");
var xml2js = require('xml2js');
var request = require('request-promise');

Promise.promisifyAll(xml2js);
Promise.promisifyAll(dd);
Promise.promisifyAll(docClient);

function Processor() {
    this.tableName = 'links-visited';

    this.putItem = function(dbItem) {
        docClient.put({
            'TableName': this.tableName,
            'Item': dbItem
        }, function(err) {
            err && console.log(err);
        });
    };

    this.getItem = function(link) {
        var dbItem = {
            'link': link
        };

        return docClient.getAsync({
            'TableName': this.tableName,
            'Key': dbItem
        });
    };

    this.processLink = function(url, entry) {
        entry.keywords = [];
        for(var keyword of this.keywords) {
            if(entry.description.search(new RegExp(keyword, 'i')) != -1) {
                entry.keywords.push(keyword);
            }
        }
        var dbItem = {
            link: url,
            title: entry.title || "Vantar fyrirsögn",
            description: entry.description || "Vantar lýsingu",
            pubDate: entry.pubDate,
            creator: entry.creator || "Vantar"
        };

        if(entry.keywords.length) {
            dbItem.isMatch = "true";
            var matchedKeywords = entry.keywords.join(',');
            dbItem.keywords = matchedKeywords;
            console.log(url, "matched", matchedKeywords);
        }
        else
        {
            dbItem.isMatch = "false";
        }
        this.putItem(dbItem);
    };

    this.processSingleEntry = function(entry) {
        var key = entry.link[0];
        this.getItem(key)
            .then(function(data) {
                if(!data.Item) {
                    var storedEntry = {
                        link: key,
                        title: entry.title[0] || "Vantar fyrirsögn",
                        description: entry.description[0] || "Vantar lýsingu",
                        pubDate: Date.parse(entry.pubDate[0]),
                        creator: entry['dc:creator'] || "Vantar"
                    };
                    console.log(key, "not found");
                    this.processLink(key, storedEntry);
                }
            }.bind(this));
    };

    this.processFeed = function(url) {
        return request(url)
            .then(function(contents) {
                return(xml2js.parseStringAsync(contents));
            })
            .then(function(result) {
                var channel = result.rss.channel[0];
                for(var entry of channel.item) {
                    this.processSingleEntry(entry);
                }
            }.bind(this))
            .catch(function(e) {
                console.log("Exception raised while processing", url);
            });
    };

    this.rescanLinks = function() {
        var params = {
            TableName: this.tableName
        };
        docClient.scanAsync(params)
            .then(function(result) {
                // TODO: handle scan exceeding limits
                for(var entry of result.Items) {
                    this.processLink(entry.link, entry);
                }
            }.bind(this));
    };
}

module.exports = {
    Processor: Processor
};