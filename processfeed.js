//add timestamps in front of log messages
require('console-stamp')(console, '[dd/mm/yyyy HH:MM:ss.l]');

var AWS = require('aws-sdk');
try {
    AWS.config.loadFromPath('./config.json');
}
catch(e) {
    AWS.config.region = "eu-west-1";
}

var processor = require("./processor.js")
var categories = require("./categories.js");

var urls = [
    "http://mbl.is/feeds/fp",
    "http://www.mbl.is/feeds/innlent/",
    "http://www.ruv.is/rss/frettir",
    "http://www.dv.is/straumur",
    "http://www.vb.is/rss/",
    "http://www.pressan.is/Rss.aspx?catID=5"
];

var processor = new processor.Processor();
processor.keywords = categories.getCombined();

var feeds = [];
for(each of urls) {
    feeds.push(processor.processFeed(each));
}
Promise.all(feeds);

